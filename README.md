# Darkcoin Masternode Deployment using vagrant and puppet

As a darkcoin user you might find this collection of scripts helpful for

* **starting a server**
* **installing darkcoin**
* **configuring darkcoin**
* **run a darkcoin masternode**

## WARNING ##
as of now this alpha and you should not move any funds, wallet keys or anything you don't want to get stolen inside the machine because

* user management is NOT properly executed nor reviewed
* ip table firewall rules are just a proof of concept
* the vagrant basebox is not checked for any backdoors
* it uses still the default ssh key which allows everyone to enter the machine
* it installs the newrelic monitoring daemon which could be a security hole

## Prerequisites

 * use linux, if you are on windows vagrant works too but some commands might differ
 * Install [vagrant](http://www.vagrantup.com/)
 * get yourset a vagrant box you trust:
 * vagrant box add precisePuppetLabs http://puppet-vagrant-boxes.puppetlabs.com/ubuntu-server-12042-x64-vbox4210.box
 * download the tar.gz archives of the puppet modules [firewall](https://forge.puppetlabs.com/puppetlabs/firewall) and [vcsrepo](https://forge.puppetlabs.com/puppetlabs/vcsrepo)
 * extract the archives and place the folders "puppetlabs-firewall-XXX" and "puppetlabs-vcsrepo-XXX" into the modules


## Deploy

 * edit the "host_environment.sh" if you want to change the name of the vagrant box, EC2 Credentials etc...
 * apply the edited data to your environment by ". host_environment.sh" # don't forget the . in the beginning
 * in the manifests folder modify the darkcoin.conf as you wish
 * run a "vagrant up" in the masternode folder
 ** you will get 4 warnings like "Warning: Firewall[001 accept all"
 ** one warning "Warning: /Stage[main]//Exec[start-newrelic]:" which is ok
 ** If you wait about 10 minutes you will get a running virtual machine with a fresh compiled darkcoin
 * connect using "vagrant ssh"
 * change the user to darkcoin: "sudo su darkcoin" and chnge into its home by "cd"
 * run "darkcoind" and then "tail -f .darkcoin/debug.log" and you should see lots of movement while it syncs


 run on amazon EC2

 * set these environment variables by sourcing them
 * ". host_environment.sh"
 * vagrant plugin install vagrant-aws
 * vagrant up --provider=aws
