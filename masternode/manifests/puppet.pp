# -*- mode: ruby -*-
# TODO use puppet module: netmanagers-fail2ban

##
#
# puppet which executes every to run a darkcoin node
#
# WARNING: use with caution because this has never been reviewed for security and therefore it is not recommend to
#   move any funds to this
#
##

###### create a darkcoin user
user { 'darkcoin_1':
  ensure     => present,
  uid        => '501',
  gid        => 'darkcoin',
  shell      => '/bin/bash',
  home       => '/home/darkcoin_1',
  managehome => true,
}
user { 'darkcoin_2':
  ensure     => present,
  uid        => '502',
  gid        => 'darkcoin',
  shell      => '/bin/bash',
  home       => '/home/darkcoin_2',
  managehome => true,
}
user { 'darkcoin_3':
  ensure     => present,
  uid        => '503',
  gid        => 'darkcoin',
  shell      => '/bin/bash',
  home       => '/home/darkcoin_3',
  managehome => true,
}
user { 'darkcoin_4':
  ensure     => present,
  uid        => '504',
  gid        => 'darkcoin',
  shell      => '/bin/bash',
  home       => '/home/darkcoin_4',
  managehome => true,
}
user { 'darkcoin_5':
  ensure     => present,
  uid        => '505',
  gid        => 'darkcoin',
  shell      => '/bin/bash',
  home       => '/home/darkcoin_5',
  managehome => true,
}
user { 'darkcoin_6':
  ensure     => present,
  uid        => '506',
  gid        => 'darkcoin',
  shell      => '/bin/bash',
  home       => '/home/darkcoin_6',
  managehome => true,
}
user { 'darkcoin_7':
  ensure     => present,
  uid        => '507',
  gid        => 'darkcoin',
  shell      => '/bin/bash',
  home       => '/home/darkcoin_7',
  managehome => true,
}

group { 'darkcoin':
  ensure     => present,
}


### update and upgrade the server
Exec {
  path    => '/usr/local/bin/:/bin/:/usr/bin/:/usr/sbin/:/sbin/',
}

group { 'puppet':
  ensure => present,
}

file { '/etc/apt/sources.list':
  ensure  => present,
  owner 	=> 'root',
  group 	=> 'root',
  mode 	=> '0644',
  source  => '/vagrant/manifests/sources.list',
}

exec { 'update':
  command => '/usr/bin/apt-get update',
}

exec { 'upgrade':
  command => '/usr/bin/apt-get upgrade -y',
  require => Exec['update']
}

exec { 'update3':
  command => '/usr/bin/apt-get update',
  require => Exec['add-bitcoin-repo'],
}


###### start installing dependencies #######
package { ['python-software-properties']:
  ensure  => installed,
  require => Exec['update', 'upgrade'],
}

exec { 'add-bitcoin-repo':
  command => 'add-apt-repository -y ppa:bitcoin/bitcoin',
  require => [Exec['update', 'upgrade'], Package['python-software-properties']]
}

package { ['git','build-essential', 'byobu', 'vim', 'nano', 'curl']:#, 'libminiupnpc8', 'libboost-all-dev', 'libminiupnpc-dev']:
  ensure  => installed,
  require => Exec['update'],
}

#
#package { ['libdb4.8-dev', 'libdb4.8++-dev']:
#  ensure  => installed,
#  require => Exec['update3'],
#}
#
###### checkout and install darkcoin but this only gives you darkcoin v0.9.XXX not v0.10.XXX
#class darkcoin_daemon {
#
#  vcsrepo { '/home/darkcoin/darkcoin_src/':
#    ensure => present,
#    provider => git,
#    source => 'https://github.com/darkcoinproject/darkcoin.git',
#    revision => 'master',
#    owner => 'darkcoin',
#    user => 'darkcoin',
#    require => [
#      Exec['update', 'upgrade', 'add-bitcoin-repo'],
#      Package['git'],
#      User['darkcoin'],
#      Group['darkcoin']
#    ],
#  }
#}
#
#include darkcoin_daemon
#
#exec { 'build_dark':
#  cwd => '/home/darkcoin/darkcoin_src/src',
#  command => '/bin/true',
#  unless => 'make -f makefile.unix', # this is bit hacky since it compiles first and then return "true" even if compiling failed
#  require => Class['darkcoin_daemon']
#}
#file { '/usr/local/bin/darkcoind':
#  ensure => 'link',
#  target => '/home/darkcoin/darkcoin_src/src/darkcoind',
#  require => Exec['build_dark']
#}

file { '/usr/local/bin/darkcoind':
  ensure => 'link',
  mode => "755",
  target => '/vagrant/darkcoind',
  require => File['/vagrant/darkcoind']
}

exec { 'get_dark':
  command => 'wget -q -O /vagrant/darkcoind https://github.com/darkcoinproject/darkcoin-binaries/blob/master/rc/darkcoind?raw=true'
}

file { '/vagrant/darkcoind':
  ensure  => present,
  owner 	=> 'vagrant',
  group 	=> 'vagrant',
  mode 	  => '0755',
  source  => '/vagrant/darkcoind',
  require => Exec['get_dark']
}



file { "/home/darkcoin_1/.darkcoin":
  ensure => "directory",
  mode => "600",
  owner        => 'darkcoin_1',
  require => File['/usr/local/bin/darkcoind']
}
file { "/home/darkcoin_2/.darkcoin":
  ensure => "directory",
  mode => "600",
  owner        => 'darkcoin_2',
  require => File['/usr/local/bin/darkcoind']
}
file { "/home/darkcoin_3/.darkcoin":
  ensure => "directory",
  mode => "600",
  owner        => 'darkcoin_3',
  require => File['/usr/local/bin/darkcoind']
}
file { "/home/darkcoin_4/.darkcoin":
  ensure => "directory",
  mode => "600",
  owner        => 'darkcoin_4',
  require => File['/usr/local/bin/darkcoind']
}
file { "/home/darkcoin_5/.darkcoin":
  ensure => "directory",
  mode => "600",
  owner        => 'darkcoin_5',
  require => File['/usr/local/bin/darkcoind']
}
file { "/home/darkcoin_6/.darkcoin":
  ensure => "directory",
  mode => "600",
  owner        => 'darkcoin_6',
  require => File['/usr/local/bin/darkcoind']
}
file { "/home/darkcoin_7/.darkcoin":
  ensure => "directory",
  mode => "600",
  owner        => 'darkcoin_7',
  require => File['/usr/local/bin/darkcoind']
}

exec {'start-darkcoind_1':
  command => 'sudo service darkcoind_1 start',
  require => [File['/vagrant/darkcoind','/usr/local/bin/darkcoind', '/etc/init.d/darkcoind_1'], User['darkcoin_1']]
}
#exec {'start-darkcoind_2':
#  command => 'sudo service darkcoind_2 start',
#  require => [File['/vagrant/darkcoind','/usr/local/bin/darkcoind', '/etc/init.d/darkcoind_2'], User['darkcoin_2']]
#}
#exec {'start-darkcoind_3':
#  command => 'sudo service darkcoind_3 start',
#  require => [File['/vagrant/darkcoind','/usr/local/bin/darkcoind', '/etc/init.d/darkcoind_3'], User['darkcoin_3']]
#}
#exec {'start-darkcoind_4':
#  command => 'sudo service darkcoind_4 start',
#  require => [File['/vagrant/darkcoind','/usr/local/bin/darkcoind', '/etc/init.d/darkcoind_4'], User['darkcoin_4']]
#}
#exec {'start-darkcoind_5':
#  command => 'sudo service darkcoind_5 start',
#  require => [File['/vagrant/darkcoind','/usr/local/bin/darkcoind', '/etc/init.d/darkcoind_5'], User['darkcoin_5']]
#}
#exec {'start-darkcoind_6':
#  command => 'sudo service darkcoind_6 start',
#  require => [File['/vagrant/darkcoind','/usr/local/bin/darkcoind', '/etc/init.d/darkcoind_6'], User['darkcoin_6']]
#}
#exec {'start-darkcoind_7':
#  command => 'sudo service darkcoind_7 start',
#  require => [File['/vagrant/darkcoind','/usr/local/bin/darkcoind', '/etc/init.d/darkcoind_7'], User['darkcoin_7']]
#}

file { '/etc/init.d/darkcoind_1':
  ensure  => present,
  owner 	=> 'vagrant',
  group 	=> 'vagrant',
  mode 	=> '0700',
  source  => '/vagrant/manifests/darkcoin/darkcoind_1',
}
file { '/etc/init.d/darkcoind_2':
  ensure  => present,
  owner 	=> 'vagrant',
  group 	=> 'vagrant',
  mode 	=> '0700',
  source  => '/vagrant/manifests/darkcoin/darkcoind_2',
}
file { '/etc/init.d/darkcoind_3':
  ensure  => present,
  owner 	=> 'vagrant',
  group 	=> 'vagrant',
  mode 	=> '0700',
  source  => '/vagrant/manifests/darkcoin/darkcoind_3',
}
file { '/etc/init.d/darkcoind_4':
  ensure  => present,
  owner 	=> 'vagrant',
  group 	=> 'vagrant',
  mode 	=> '0700',
  source  => '/vagrant/manifests/darkcoin/darkcoind_4',
}
file { '/etc/init.d/darkcoind_5':
  ensure  => present,
  owner 	=> 'vagrant',
  group 	=> 'vagrant',
  mode 	=> '0700',
  source  => '/vagrant/manifests/darkcoin/darkcoind_5',
}
file { '/etc/init.d/darkcoind_6':
  ensure  => present,
  owner 	=> 'vagrant',
  group 	=> 'vagrant',
  mode 	=> '0700',
  source  => '/vagrant/manifests/darkcoin/darkcoind_6',
}
file { '/etc/init.d/darkcoind_7':
  ensure  => present,
  owner 	=> 'vagrant',
  group 	=> 'vagrant',
  mode 	=> '0700',
  source  => '/vagrant/manifests/darkcoin/darkcoind_7',
}


## initialize the dark coin configuration file
file { '/home/darkcoin_1/.darkcoin/darkcoin.conf':
  ensure  => present,
  owner 	=> 'darkcoin_1',
  group 	=> 'darkcoin',
  mode 	=> '0444',
  source  => '/vagrant/manifests/darkcoin/darkcoin_1.conf',
  require => [User['darkcoin_1']]
}
file { '/home/darkcoin_1/.darkcoin/peers.dat':
  ensure  => present,
  owner 	=> 'darkcoin_1',
  group 	=> 'darkcoin',
  mode 	=> '0444',
  source  => '/vagrant/manifests/darkcoin/peers.dat',
  require => [User['darkcoin_1']]
}
file { '/home/darkcoin_2/.darkcoin/darkcoin.conf':
  ensure  => present,
  owner 	=> 'darkcoin_2',
  group 	=> 'darkcoin',
  mode 	=> '0444',
  source  => '/vagrant/manifests/darkcoin/darkcoin_2.conf',
  require => [User['darkcoin_2']]
}
file { '/home/darkcoin_3/.darkcoin/darkcoin.conf':
  ensure  => present,
  owner 	=> 'darkcoin_3',
  group 	=> 'darkcoin',
  mode 	=> '0444',
  source  => '/vagrant/manifests/darkcoin/darkcoin_3.conf',
  require => [User['darkcoin_2']]
}
file { '/home/darkcoin_4/.darkcoin/darkcoin.conf':
  ensure  => present,
  owner 	=> 'darkcoin_4',
  group 	=> 'darkcoin',
  mode 	=> '0444',
  source  => '/vagrant/manifests/darkcoin/darkcoin_4.conf',
  require => [User['darkcoin_2']]
}
file { '/home/darkcoin_5/.darkcoin/darkcoin.conf':
  ensure  => present,
  owner 	=> 'darkcoin_5',
  group 	=> 'darkcoin',
  mode 	=> '0444',
  source  => '/vagrant/manifests/darkcoin/darkcoin_5.conf',
  require => [User['darkcoin_2']]
}
file { '/home/darkcoin_6/.darkcoin/darkcoin.conf':
  ensure  => present,
  owner 	=> 'darkcoin_6',
  group 	=> 'darkcoin',
  mode 	=> '0444',
  source  => '/vagrant/manifests/darkcoin/darkcoin_6.conf',
  require => [User['darkcoin_2']]
}
file { '/home/darkcoin_7/.darkcoin/darkcoin.conf':
  ensure  => present,
  owner 	=> 'darkcoin_7',
  group 	=> 'darkcoin',
  mode 	=> '0444',
  source  => '/vagrant/manifests/darkcoin/darkcoin_7.conf',
  require => [User['darkcoin_2']]
}


###### install newrelic to do monitoring on your server
exec { 'update2':
  command => '/usr/bin/apt-get update',
  require => [File['/etc/apt/sources.list'],Exec['add-newrelic-repo-key']],
}

exec {'add-newrelic-repo-key':
  command => 'wget -O- https://download.newrelic.com/548C16BF.gpg | apt-key add -',
  require => [File['/etc/apt/sources.list'], Package['curl']]
}

package { 'newrelic-sysmond':
  ensure  => installed,
  require => Exec['update2'],
}

exec {'set-newrelic-license-key':
  command => 'nrsysmond-config --set license_key=xyz', ### todo use your own here
  require => [Package['newrelic-sysmond']],
}

exec {'start-newrelic':
  command => '/etc/init.d/newrelic-sysmond start',
  require => [Exec['set-newrelic-license-key']],
}


##### firewall setup #####
resources { "firewall":
  purge => true
}

Firewall {
  require => Class['firewall_rules'],
}


class firewall_rules {
  Firewall {
    require => undef,
  }
# Default firewall rules
firewall { '000 accept all icmp':
  proto   => 'icmp',
  action  => 'accept',
}->
firewall { '001 accept all to lo interface':
  proto   => 'all',
  iniface => 'lo',
  action  => 'accept',
}->
firewall { '002 accept related established rules':
  proto   => 'all',
  ctstate => ['RELATED', 'ESTABLISHED'],
  action  => 'accept',
}->

  # Default firewall rules
  firewall { '003 accept ssh and darkcoin daemon connection':
    port   => [22,9991,9992,9993,9994,9995,9996,9997,9998,9999],
    proto  => tcp,
    action => accept,
  }->
  firewall { "999 drop all other requests":
    action => "drop"
  }

}
include firewall_rules


